package com.android.eegplayer.item;

import android.graphics.Bitmap;
import android.net.Uri;

public class MusicItem {
	public String artist;
	public String title;
	public Uri uri;
	public Bitmap img;
	public String path;
}
