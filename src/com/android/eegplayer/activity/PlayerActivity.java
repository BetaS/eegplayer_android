package com.android.eegplayer.activity;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.ListIterator;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.eegplayer.R;
import com.android.eegplayer.item.MusicItem;

public class PlayerActivity extends Activity implements OnClickListener, OnCompletionListener {
	protected MediaPlayer player = new MediaPlayer();
	protected ArrayList<MusicItem> item = new ArrayList<MusicItem>();
	protected int curr_idx = -1;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_2_player);
		
		findViewById(R.id.btn_play).setOnClickListener(this);
		findViewById(R.id.btn_backward).setOnClickListener(this);
		findViewById(R.id.btn_forward).setOnClickListener(this);
		
        player.setOnCompletionListener(this);
		String artist = getIntent().getStringExtra("artist");
		
		String[] projection = { MediaStore.Audio.Media._ID, MediaStore.Audio.Media.DISPLAY_NAME, MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.ALBUM_ID, MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.ARTIST };
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!=" + 0 + " and "+MediaStore.Audio.Media.ARTIST + "== \""+artist+"\"";
        Cursor cursor = managedQuery(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, projection, selection, null, MediaStore.Audio.Media.ARTIST+", "+MediaStore.Audio.Media.TITLE + " ASC");
        
        Log.e("test", "find...");
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            int idx1 = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST);
            int idx2 = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE);
            int idx3 = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID);
            int idx4 = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
            
            MusicItem i = new MusicItem();
            i.artist = cursor.getString(idx1);
            i.title = cursor.getString(idx2);  
            
            Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
            Uri uri = ContentUris.withAppendedId(sArtworkUri, cursor.getLong(idx3));

            i.uri = uri;
            i.path = cursor.getString(idx4);
            
            item.add(i);
        }
        playnext();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		
		player.stop();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		player.release();
	}
	
	public void playnext() {
		curr_idx++;
		if(curr_idx >= item.size()) {
			curr_idx = 0;
		}
		play();
	}

	public void playprev() {
		curr_idx--;
		if(curr_idx < 0) {
			curr_idx = item.size()-1;
		}
		play();
	}

	public void play() {
		if(player.isPlaying()) {
			player.stop();
			player.reset();
		}

		
		MusicItem i = item.get(curr_idx);
		
		setMusicData(i);
		
        try {
			player.setDataSource(i.path);
			player.prepare();
	        player.start();
		}  catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setMusicData(MusicItem i) {
		Bitmap artwork = null;
		try {
			ContentResolver res = getContentResolver();
			InputStream in = res.openInputStream(i.uri);
			artwork = BitmapFactory.decodeStream(in);
		} catch(IOException e) {
			
		}
		
		((ImageView)findViewById(R.id.img)).setImageBitmap(artwork);
		((TextView)findViewById(R.id.txt_artist)).setText(i.artist);
		((TextView)findViewById(R.id.txt_title)).setText(i.title);
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		playnext();
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.btn_play) {
			if(player.isPlaying())
				player.pause();
			else
				player.start();
		} else if(v.getId() == R.id.btn_forward) {
			playnext();
		} else if(v.getId() == R.id.btn_backward) {
			playprev();
		}
	}
}
