package com.android.eegplayer.activity;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.android.eegplayer.R;
import com.android.eegplayer.item.ArtistItem;
import com.android.eegplayer.item.MusicItem;
import com.android.eegplayer.util.CustomAdapter;
import com.android.eegplayer.util.CustomAdapter.ViewHolder;

public class MainActivity extends Activity implements OnItemClickListener {
	public static Bitmap no_img = null;
	
	protected AbsListView listview = null;
	protected CustomAdapter<MusicItem> adapter = null;
	protected ArrayList<MusicItem> data = new ArrayList<MusicItem>();
	protected int item_layout = R.layout.item_music;

	protected HashMap<String, ArtistItem> items = new HashMap<String, ArtistItem>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_1_list);
		
		no_img = BitmapFactory.decodeResource(getResources(), R.drawable.no_img);

		listview = (AbsListView)findViewById(android.R.id.list);
		
		adapter = new CustomAdapter<MusicItem>(this, item_layout, data) {
			@Override
			public ViewHolder createHolder(View view) {
				return createListHolder(view);
			}
			@Override
			public void updateView(int idx, ViewHolder holder, MusicItem item) {
				updateListItem(idx, holder, item);
			}
		};
		
		listview.setAdapter(adapter);
		listview.setOnItemClickListener(this);
	
		// 음악 셀렉트
		
		String[] projection = { MediaStore.Audio.Media._ID, MediaStore.Audio.Media.DISPLAY_NAME, MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.ALBUM_ID, MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.ARTIST };
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!=" + 0;
        Cursor cursor = managedQuery(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, projection, selection, null, MediaStore.Audio.Media.ARTIST+", "+MediaStore.Audio.Media.TITLE + " ASC");
        
        Log.e("test", "find...");
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            int idx1 = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST);
            int idx2 = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE);
            int idx3 = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID);
            
            Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
            Uri uri = ContentUris.withAppendedId(sArtworkUri, cursor.getLong(idx3));

            String artist = cursor.getString(idx1);
        	ArtistItem item = items.get(artist);
            if(item == null) {
            	item = new ArtistItem();
            	item.cnt = 0;
            	item.uri = uri;
            }
            
        	item.cnt++;
        	
        	items.put(artist, item);
        }

        Log.e("test", "sort...");
        ArrayList<MusicItem> d = new ArrayList<MusicItem>();
        for(String artist: new TreeSet<String>(items.keySet())) {
        	MusicItem item = new MusicItem();
        	item.artist = artist;
        	item.title = items.get(artist).cnt+"개의 곡";
        	item.uri = items.get(artist).uri;
        	
        	d.add(item);
        }
        Log.e("test", "ok...");
        
        setListData(d);
	}

	protected ViewHolder createListHolder(View view) {
		ViewHolder holder = new ViewHolder();
		
		holder.put("img", view.findViewById(R.id.img));
		holder.put("artist", view.findViewById(R.id.txt_artist));
		holder.put("title", view.findViewById(R.id.txt_title));
		
		return holder;
	}
	protected void updateListItem(int idx, ViewHolder holder, MusicItem item) {
		holder.getTextView("artist").setText(item.artist);
		holder.getTextView("title").setText(item.title);
		
    	if(item.img == null) {
    		try {
    			ContentResolver res = getContentResolver();
    			InputStream in = res.openInputStream(item.uri);
    			item.img = BitmapFactory.decodeStream(in);
    		} catch(FileNotFoundException e) {
    			item.img = no_img;
    		}
    	}
		holder.getImageView("img").setImageBitmap(item.img);
		
		
	}

	public void setListData(ArrayList<MusicItem> data) {
		this.data.clear();
		this.data.addAll(data);

		adapter.notifyDataSetChanged();
	}
	public void resetList() {
		this.data.clear();
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		MusicItem item = data.get(position);
		
		Intent intent = new Intent(this, PlayerActivity.class);
		intent.putExtra("artist", item.artist);
		startActivity(intent);
	}
}
